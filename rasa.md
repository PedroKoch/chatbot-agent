## Introdução ao [Rasa](https://rasa.com/docs/rasa/)

A biblioteca de código aberto `Rasa` é utilizada para o desenvolvimento de agentes conversacionais (chatbots) e 
inclui dois módulos principais:
 - Compreensão de Linguagem Natural (NLU): captura informação contextual e determina a intenção do usuário;
 - Núcleo (Core): seleciona a melhor resposta ou ação basedo no histórico de conversação.
 

Os agentes possuem uma arquitetura reativa, ou seja, o fluxo de seu processamento se inicia ao receber uma mensagem do 
usuário. A mensagem recebida é entregue ao componente Interpretador que, apoiado pelo módulo de NLU, é responsável por 
coletar informações como o testo original, a intenção da mensagem e as entidades reconhecidas. De seguida, essas 
informações são passadas ao componente Rastreador, que mantém o registro dos estados do agente ao longo da conversa. 
Por sua vez, o componente Política, dado o estado da conversa, decide a Ação a ser tomada pelo agente. O último 
componente,  a Ação, consiste em responder o usuário e mandar informações sobre a ação tomada ao Rastreador.


### Conceitos

 - Pipeline:
    - é uma sequência de (sub)Componentes pelos quais uma mensagem recebida passa, interna ao Componente Interpretador.
    - essa sequência consiste de três partes principais: Segmentação, Criação de Features e terceira pode ser um 
    subconjunto das tarefas Reconhecimento de Entidades, Classificação de Intenção e Seletor de Respostas.
 - Intenções:
    - agrupam trechos de mensagens com o mesmo significado.
    - definem os exemplos de treinamento do módulo de NLU.
    - são um output da Pipeline do Interpretador.
 - Ações:
    - representam o elemento a ser predito pelo modelo dos agentes e englobam uma ou mais respostas a ser enviadas ao 
    usuário.
    - podem ser generalizadas para ações onde o agente interage com outros sistemas (através de uma API, por exemplo).
 - Respostas:
    - são usadas pelas Ações para responder ao usuário.
 - Histórias:
     - simulam conversas reais com o usuário, onde no lugar de uma mensagem de usuário indica-se a intenção (e 
     possíveis entidades) e no lugar de uma resposta específica do agente, indica-se a Ação a ser tomada.
 - Domínio:
    - define o universo onde o agente se localiza, listando intenções, ações e respostas
 - Políticas:
    - modelos responsáveis por decidir a melhor Ação a cada passo de uma conversa.
    - um agente pode possuir mais uma Política. Assim, decide a próxima ação seguindo uma hierarquia de prioridades 
    (cada Política possui um nível de prioridade).
    - uma Ação predita por uma Política só é executada se a confiança nessa predição for acima de um limite 
    (configurável).
    - um exemplo de Política embutida na biblioteca Rasa é a `Keras Policy` cuja arquitetura padrão é uma rede neural 
    baseada em LSTM (Long Short-Term Memory).

