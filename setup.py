#!/usr/bin/env python
from os.path import join
from typing import List

from setuptools import setup, find_packages

# Repository name
REPO_NAME = 'chatbot-agent'

# How this module is imported in Python (name of the folder inside `src`)
MODULE_NAME_IMPORT = 'chatbot_rasa'

MODULE_NAME = 'chatbot_rasa'

# File to get direct dependencies from (used by pip)
REQUIREMENTS_FILE = 'requirements.txt'


def requirements_from_pip(filename: str) -> List[str]:
    with open(filename, 'r') as pip:
        return [ln.strip() for ln in pip if not ln.startswith('#') and ln.strip()]


SETUP_ARGS = {
    'name': MODULE_NAME,
    'description': '',
    'url': 'https://gitlab.com/PedroKoch/{:s}'.format(REPO_NAME),
    'author': 'Henrique Donancio, Pedro Koch',
    'package_dir': {'': 'src'},
    'packages': find_packages('src'),
    'version': (open(join('src', MODULE_NAME_IMPORT, 'resources', 'VERSION')).read().strip()),
    'install_requires': requirements_from_pip(REQUIREMENTS_FILE),
    'extras_require': {
        'test_deps': requirements_from_pip('requirements_test.txt')},
    'include_package_data': True,
    'zip_safe': False,
    'classifiers': [
        'Programming Language :: Python :: 3.6']
}

if __name__ == '__main__':
    setup(**SETUP_ARGS)
