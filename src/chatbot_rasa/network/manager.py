from spade.agent import Agent
from spade.behaviour import OneShotBehaviour
from spade.message import Message


class NetworkManager(Agent):
    class SetupBehaviour(OneShotBehaviour):
        async def run(self):
            print(f"[NetworkManager {str(self.agent.jid)}]: SetupBehaviour starting")
            self.agent.add_behaviour(self.agent.SendMessage())

    class SendMessage(OneShotBehaviour):
        async def run(self):
            print(f"[NetworkManager {str(self.agent.jid)}]: SetupBehaviour starting")
            msg = Message(to="networkcoordinator@localhost")  # Instantiate the message
            msg.set_metadata("performative", "inform")  # Set the "inform" FIPA performative
            msg.set_metadata("ontology", "myOntology")  # Set the ontology of the message content
            msg.set_metadata("language", "OWL-S")  # Set the language of the message content
            msg.body = f"{self.agent.get('agent_name')} ready to work!!"  # Set the message content
            await self.send(msg)

    def __init__(self, jid, password, info: dict, alias: str = ""):
        Agent.__init__(self, jid, password)
        self.set("alias", alias)
        for k, v in info.items():
            self.set(k, v)

    async def setup(self):
        print(f"[Agent NetworkManager {str(self.jid)} is ready]")
        b = self.SetupBehaviour()
        self.add_behaviour(b)
        self.web.start(hostname="127.0.0.1", port=str(10000 + int(self.get("alias")[-2:])))
