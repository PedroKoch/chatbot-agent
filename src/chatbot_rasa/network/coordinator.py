import time
import subprocess
import yaml
from spade.agent import Agent
from spade.behaviour import OneShotBehaviour
from spade.message import Message
from chatbot_rasa.protocols.generic.subscribe import SubscribeXMPP, CreateManager


class NetworkCoordinator(Agent):
    # This behaviour start coverage area management reading server-settings.yaml file and configuring the
    #  system based on the description of this
    class SetupBehaviour(OneShotBehaviour):
        async def run(self):
            msg = Message(to="usr1@localhost")  # Instantiate the message
            msg.set_metadata("performative", "inform")  # Set the "inform" FIPA performative
            msg.set_metadata("ontology", "myOntology")  # Set the ontology of the message content
            msg.set_metadata("language", "OWL-S")  # Set the language of the message content
            msg.body = "Hello World"  # Set the message content

            await self.send(msg)
            print("[NetworkCoordinator]: SetupBehaviour starting")

            config = read_config()
            host = config["server"]["host"]
            self.agent.set("server", host.lstrip("@") if host.startswith("@") else host)
            self.agent.set("password", config["server"]["password"])
            print(f"I am in {self.agent.get('server')} and I'll register my managers here too. :)")

            self.agent.set("managed_areas", config["areas"])
            preexisting_behaviors = self.agent.behaviours
            print(f"Managed areas: {self.agent.get('managed_areas')}")
            print(f"Preexisting behaviors: {preexisting_behaviors}")

            for i, area in enumerate(config["areas"]):
                agent_name = area.lower().replace(" ", "_")
                num_manager = str(i + len(preexisting_behaviors)).rjust(2, '0')
                print(f"num manager {num_manager}")
                self.agent.set(
                    f"Manager_{num_manager}", {
                        "id": num_manager,
                        "agent_name": agent_name,
                        "agent_host": self.agent.get('server'),
                        "agent_full_name": f"{agent_name}@{self.agent.get('server')}",
                        "password": f"{config['server']['password']}{num_manager}",
                        "area_name": area,
                        "sub_areas": config[area]
                    })
                print(f"[NetworkCoordinator]: a new agent ({agent_name}) will be subscribed in the XMPP server.")
                # Subscribe a new user in XMPP server (Prosody) by bash command
                b1 = SubscribeXMPP(f"Manager_{num_manager}")
                self.agent.add_behaviour(b1)
                # Create a new NetworkManager agent
                b2 = CreateManager(f"Manager_{num_manager}")
                self.agent.add_behaviour(b2)

    async def setup(self):
        print(f"[Agent NetworkCoordinator ({str(self.jid)}) is ready]")
        print(self.is_alive())
        b = self.SetupBehaviour()
        self.add_behaviour(b)
        print(self.behaviours)
        self.web.start(hostname="127.0.0.1", port="10000")


def read_config(yaml_file_path: str = "src/chatbot_rasa/resources/server-settings.yaml") -> dict:
    with open(yaml_file_path) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    return config


if __name__ == "__main__":
    command = "sudo prosodyctl register networkcoordinator localhost pedaneel"
    print(command)
    print(subprocess.getstatusoutput(command))
    network_coordinator = NetworkCoordinator("networkcoordinator@localhost", "pedaneel")
    network_coordinator.start()
    time.sleep(1)
    print(network_coordinator.is_alive())
