import os
import logging
import asyncio

from rasa.core.agent import Agent
from rasa.core.policies.keras_policy import KerasPolicy
from rasa.core.policies.memoization import MemoizationPolicy
from rasa.core.interpreter import RasaNLUInterpreter
from rasa.core.utils import EndpointConfig
from rasa.core.run import serve_application
from rasa.core import config

logger = logging.getLogger(__name__)

RESOURCES_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "resources")
MODEL_PATH = os.path.join(RESOURCES_PATH, "models/dialogue")
DOMAIN_FILE = os.path.join(RESOURCES_PATH, "energy-domain.yml")
DATA_FILE = os.path.join(RESOURCES_PATH, "data/stories.md")


async def train_dialogue(domain_file = DOMAIN_FILE, model_path = MODEL_PATH, training_data_file = DATA_FILE):
	agent = Agent(domain_file, policies = [MemoizationPolicy(), KerasPolicy(max_history=3, epochs=200, batch_size=50)])
	data = await agent.load_data(training_data_file)
	agent.train(data)
	agent.persist(model_path)
	return agent


async def run_energy_bot():
	interpreter = RasaNLUInterpreter('./models/nlu/default/chatbot')
	action_endpoint = EndpointConfig(url="http://localhost:5055/webhook")
	agent = Agent.load(model_path=MODEL_PATH, interpreter=interpreter, action_endpoint=action_endpoint)
	await agent.handle_text("hello")
	# serve_application(agent, channel='cmdline')
	print(agent)
	return agent


async def main():
	await train_dialogue()
	print("Train done.")
	await run_energy_bot()


if __name__ == '__main__':
	x = main()
	print(x)
