import logging

from rasa.core.agent import Agent
from rasa.core.policies.keras_policy import KerasPolicy
from rasa.core.policies.memoization import MemoizationPolicy
from rasa.core.interpreter import RasaNLUInterpreter
from rasa.core.training import interactive
from rasa.core.utils import EndpointConfig

logger = logging.getLogger(__name__)


def run_energy_online(interpreter, domain_file="energy-domain.yml", training_data_file='data/stories.md'):
    action_endpoint = EndpointConfig(url="http://localhost:5055/webhook")
    agent = Agent(domain_file,
                  policies=[MemoizationPolicy(max_history=20), KerasPolicy(max_history=20, epochs=10, batch_size=700)],
                  interpreter=interpreter,
                  action_endpoint=action_endpoint)

    data = agent.load_data(training_data_file)
    agent.train(data)
    interactive.run_interactive_learning(agent, training_data_file)
    return agent


if __name__ == '__main__':
    logging.basicConfig(level="INFO")
    nlu_interpreter = RasaNLUInterpreter('./models/nlu/default/chatbot')
    run_energy_online(nlu_interpreter)
