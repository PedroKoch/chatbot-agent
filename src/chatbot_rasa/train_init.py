import logging

from rasa.core.agent import Agent
from rasa.core.policies.keras_policy import KerasPolicy
from rasa.core.policies.memoization import MemoizationPolicy

logger = logging.getLogger(__name__)


def train_dialogue(domain_file='energy-domain.yml', model_path='./models/dialogue',
                   training_data_file='./data/stories.md'):
    agent = Agent(domain_file, policies=[MemoizationPolicy(), KerasPolicy(max_history=20, epochs=400, batch_size=700)])
    data = agent.load_data(training_data_file)

    agent.train(data)

    agent.persist(model_path)
    return agent


if __name__ == '__main__':
    train_dialogue()
