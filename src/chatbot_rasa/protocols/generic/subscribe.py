import subprocess
from spade.behaviour import OneShotBehaviour
from chatbot_rasa.network.manager import NetworkManager


class SubscribeXMPP(OneShotBehaviour):

    def __init__(self, manager: str):
        OneShotBehaviour.__init__(self)
        self.manager_name = manager

    async def run(self):
        print("[NetworkCoordinator]: SubscribeXMPP starting")
        agent_info = self.agent.get(self.manager_name)
        print(f"Subscribing agent {agent_info['agent_name']} at {agent_info['agent_host']}")

        # Bash commmand to register a new user in Prosody: user server password
        command = \
            f"sudo prosodyctl register {agent_info['agent_name']} {agent_info['agent_host']} {agent_info['password']}"
        print(command)
        print(subprocess.getstatusoutput(command))

    async def on_end(self):
        print("[NetworkCoordinator]: Agent JID subscribed in Prosody XMPP server.")


class CreateManager(OneShotBehaviour):
    def __init__(self, manager: str):
        OneShotBehaviour.__init__(self)
        self.manager_name = manager

    async def run(self):
        print(self.agent.behaviours)
        print([b.manager_name for b in self.agent.behaviours if hasattr(b, "manager_name")])
        search_subscribe_behavior = [(i, b.manager_name, b)
                                     for i, b in enumerate(self.agent.behaviours)
                                     if hasattr(b, "manager_name") and b.manager_name == self.manager_name]
        print(f"Creating spade agent {self.manager_name} (Coordinator has {len(self.agent.behaviours)} behaviours)")
        print(search_subscribe_behavior)
        await self.agent.behaviours[search_subscribe_behavior[0][0]].join()

        agent_info = self.agent.get(self.manager_name)
        # Create a new NetworkManager SPADE agent
        agent2 = NetworkManager(jid=agent_info['agent_full_name'], password=agent_info['password'],
                                info=agent_info, alias=self.manager_name)  # , loop=self.agent.loop
        await agent2.start(auto_register=False)

    async def on_end(self):
        print("[NetworkCoordinator]: New NetworkManager agent was created in SPADE.")
