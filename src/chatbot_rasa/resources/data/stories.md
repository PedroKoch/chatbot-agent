## Generated Story -7872591835032529080
* greet
    - utter_greet
* inform
    - utter_ask_registry
* inform{"registry": "100000"}
    - slot{"registry": "100000"}
    - action_check
    - slot{"location": "Campo Belo"}
* agree
    - utter_ask_how_long
* inform
    - utter_ask_pre_service
* refuse
    - utter_ask_infrastructure
* refuse
	- utter_ask_burst		
* refuse
    - utter_ask_check
* agree
    - utter_start_service
    - action_sendAssistance
* goodbye
    - utter_goodbye

## Generated Story -5872591835932529080
* greet
    - utter_greet
* inform
    - utter_ask_registry
* inform{"registry": "100000"}
    - slot{"registry": "100000"}
    - action_check
    - slot{"location": "Campo Belo"}
* agree
    - utter_ask_how_long
* inform
    - utter_ask_pre_service
* refuse
    - utter_ask_infrastructure
* refuse
    - utter_ask_burst       
* refuse
    - utter_ask_check
* refuse
    - utter_goodbye

## Generated Story -5822591825932569080
* greet
    - utter_greet
* inform
    - utter_ask_registry
* inform{"registry": "100000"}
    - slot{"registry": "100000"}
    - action_check
    - slot{"location": "Campo Belo"}
* agree
    - utter_ask_how_long
* inform
    - utter_ask_pre_service
* refuse
    - utter_ask_infrastructure
* agree
    - utter_ask_infrastructure_
* inform
    - utter_ask_burst       
* agree
    - utter_start_service
    - action_sendAssistance
* goodbye
    - utter_goodbye

## Generated Story -5654005946382616323
* greet
    - utter_greet
* inform
    - utter_ask_registry
* inform{"registry": "100000"}
    - slot{"registry": "100000"}
    - action_check
    - slot{"location": "Campo Belo"}
* agree
    - utter_ask_how_long
* inform
    - utter_ask_pre_service
* refuse
    - utter_ask_infrastructure
* refuse
    - utter_ask_burst 
* agree
    - utter_start_service
    - action_sendAssistance
* goodbye
    - utter_goodbye

## Generated Story -7650937089471439221
* greet
    - utter_greet
* inform
    - utter_ask_registry
* inform{"registry": "100000"}
    - slot{"registry": "100000"}
    - action_check
    - slot{"location": "Campo Belo"}
* agree
    - utter_ask_how_long
* inform
    - utter_ask_pre_service
* refuse
    - utter_ask_infrastructure
* refuse
    - utter_ask_burst
* refuse
    - utter_ask_check
* agree
    - utter_start_service
    - action_sendAssistance
* goodbye
    - utter_goodbye

## Generated Story 2760669639753064217
* greet
    - utter_greet
* inform
    - utter_ask_registry
* inform{"registry": "100001"}
    - slot{"registry": "100001"}
    - action_check
    - slot{"location": "Cachoeirinha"}
* agree
    - utter_ask_how_long
* inform
    - utter_ask_pre_service
* refuse
    - utter_ask_infrastructure
* refuse
    - utter_ask_burst
* agree
    - utter_start_service
    - action_sendAssistance
* goodbye
    - utter_goodbye

## Generated Story 7173706399213010359
* greet
    - utter_greet
* inform{"location": "Cachoeirinha"}
    - slot{"location": "Cachoeirinha"}
    - utter_ask_registry
* inform{"registry": "100001"}
    - slot{"registry": "100001"}
    - action_check
    - slot{"location": "Cachoeirinha"}
* agree
    - utter_ask_how_long
* inform
    - utter_ask_pre_service
* refuse
    - utter_ask_infrastructure
* refuse
    - utter_ask_burst
* agree
    - utter_start_service
    - action_sendAssistance
* goodbye
    - utter_goodbye

## Generated Story 8843927928658246887
* greet
    - utter_greet
* inform
    - utter_ask_registry
* inform{"registry": "100101"}
    - slot{"registry": "100101"}
    - action_check
    - slot{"location": "Tucuruvi"}
* agree
    - utter_ask_how_long
* inform
    - utter_ask_pre_service
* agree
    - utter_ask_infrastructure
* agree    
    - utter_ask_infrastructure_
* inform
    - utter_ask_burst       
* agree
    - utter_start_service
    - action_sendAssistance
* goodbye
    - utter_goodbye

## Generated Story 8373124682682373365
* greet
    - utter_greet
* inform
    - utter_ask_registry
* inform{"registry": "200001"}
    - slot{"registry": "200001"}
    - action_check
    - slot{"location": "Liberdade"}
* agree
    - utter_ask_how_long
* inform
    - utter_ask_pre_service
* agree
    - utter_ask_infrastructure
* refuse    
    - utter_ask_burst       
* agree
    - utter_start_service
    - action_sendAssistance
* goodbye
    - utter_goodbye

## Generated Story -8366816819277778407
* greet
    - utter_greet
* inform{"location": "Liberdade"}
    - slot{"location": "Liberdade"}
    - utter_ask_registry
* inform{"registry": "200001"}
    - slot{"registry": "200001"}
    - action_check
    - slot{"location": "Liberdade"}
* agree
    - utter_ask_how_long
* inform
    - utter_ask_pre_service
* refuse
    - utter_ask_infrastructure
* refuse
    - utter_ask_burst
* agree
    - utter_start_service
    - action_sendAssistance
* goodbye
    - utter_goodbye

## Generated Story 3129971223741376509
* greet
    - utter_greet
* inform
    - utter_ask_registry
* inform{"registry": "200001"}
    - slot{"registry": "200001"}
    - action_check
    - slot{"location": "Liberdade"}
* agree
    - utter_ask_how_long
* inform
    - utter_ask_pre_service
* refuse
    - utter_ask_infrastructure
* agree
    - utter_ask_infrastructure_
* inform
    - utter_ask_burst       
* refuse
    - utter_start_service
    - action_sendAssistance
* goodbye
    - utter_goodbye
