import psycopg2 as psql

from rasa_sdk import Action
from rasa_sdk.events import SlotSet

class ActionCheck(Action):
	def name(self):
		return 'action_check'
		
	def run(self, dispatcher, tracker, domain):
		
		reg = tracker.get_slot('registry')

		try:
			connect_str = "dbname='chatbot' user='chatbot_app' host='localhost' password='chatbot'"
			# use our connection values to establish a connection
			conn = psql.connect(connect_str)
			# create a psycopg2 cursor that can execute queries
			cursor = conn.cursor()
			
			cursor.execute(f'SELECT "NAME" from chatbot where "REGISTRY" = {reg};')
			user = cursor.fetchall()
			if len(user) > 0:
				user = str(user[0][0]) if len(user[0]) > 0 else None

		except Exception as e:
			print("Uh oh, não é possível conectar. dbname, usuário ou senha inválidos?")
			user = cursor = None
			print(e)

		if user:
			cursor.execute(f'SELECT "LOCATION" from chatbot where "REGISTRY" = {reg};')
			loc = cursor.fetchall()
			if len(loc) > 0:
				loc = str(loc[0][0]) if len(loc[0]) > 0 else None
			response = f"Obrigado Sr./Sra. {user}.\nO endereço {loc} está correto?"
		else:
			response = "Esse número de instalação não consta em nossa base de dados, por favor insira novamente o " \
					   "número de instalação"

		dispatcher.utter_message(response)
		return [SlotSet('location', loc)]

class ActionSendAssistance(Action):
	def name(self):
		return 'action_sendAssistance'

	def run(self, dispatcher, tracker, domain):
		print("Enviando assistência")
