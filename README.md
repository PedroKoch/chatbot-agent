# Chatbot Agent

Antes de começar: é importante baixar o pacote português no spacy e linkar ele com o comando abaixo:

$ python3 -m spacy link pt_core_news_sm pt

1º . Treinar o modelo. 

Na pasta 'data' está o arquivo data.json. É nele onde estão as informações iniciais que serão treinadas no modelo. Para editá-las diretamente com o Rasa
vá até .../data e execute no terminal:

$ rasa-nlu-trainer

O arquivo data.json contém os dados de treinamento. Para esse exemplo usou-se três objetivos comunicativos: greet, goodbye e inform.

2º . Para treinar o modelo execute o comando abaixo. As configurações (config_spacy.json) e parâmetros de entradas de modelo (nlu_model.py)

$ python3 nlu_model.py

3º . A próxima etapa é criar estados ou fluxos que a comunicação deve seguir. Para isso cria-se um domínio para o problema em questão, nesse caso, energy_domain.yml. Baseado no contexto e estado da conversa, o Rasa pode prever a próxima ação do chatbot.

4º . Algumas respostas precisam de operações complexas como consulta em bancos de dados. Para tal, uma simples resposta de texto não é suficiente, portanto é preciso criar uma ação mais complexa. O arquivo
actions.py implementa essas ações (consulta à base de dados e coomunicação SMA).

Utilizando Postgresql:

$pip install psycopg2

5º . Parte do treinamento é estabelecer uma ordem para os estados criados na etapa 3, ou seja, os fluxos. O arquivo stories.md estabelece esses fluxos. Inicialmente podemos estabelecer um fluxo esperado e posteriormente fazer um treinamento online cobrindo possibilidades desses fluxos. Para fazer o treinamento inicial com os fluxos dados:

$ train_init.py

6º . Agora temos um modelo "pré-treinado", é hora de testar esses fluxos e incrementá-los com um treinamento online. Antes disso, é preciso "linkar" nossas custom actions ao domínio.

$ python3 -m rasa_core_sdk.endpoint --actions actions

E então executar o treinamento online interativo (em outra aba):

$ python3 train_online.py
